#ifndef VECTOR_H
#define VECTOR_H
#include <vector>


class Vector
{
    public:
    //construction
        Vector(int taille);
        float afficheElement(int valeur);
        void supElement(int indice);
        void ajoutElement(int valeur, float nvValeur);
        Vector sommeVecteur(Vector vec2);
        int getSize();
        virtual ~Vector();


    private:

        std::vector<float> monVec;
};

#endif // VECTOR_H
